# Onboard

## Problem

We currently use a Confluence template when off-boarding staff.  The template is used to create a page that has tasks
that a few teams need to complete when off-boarding staff, and there is no template for on-boarding staff.  Maintaining
the template is awkward and no one person has responsibility for it.  There is a risk that the Confluence page can be
modified by anyone, such as checking/unchecking tasks and there is little auditing.

## Solution

This service integrates with Jira to create a story for each staff member that is on-boarded.  The story contains
subtasks for each access that is needed, that are tailored to each role.

## Documentation

### Startup

When the application first starts it checks that the configured board exists.  If it does not exist, the application
creates the board and statuses.  If the board exists, the application checks that the correct statuses exist and throws
an exception if not.

The environment variables need to be set in order to start the application:

* `JIRA_API_TOKEN`
* `JIRA_API_URL`
* `JIRA_API_USERNAME`

### Authentication

The web application uses basic authentication to log in to Jira, using a username and API key.  

### Interface

The web interface uses Thymeleaf for page templating and Bootstrap for layout and styling.

### Endpoints

## TODO

This work is incomplete so this is a list of some of the work that could/should be done:

* Rebuild as a Forge app.
* Use OAuth2 to authenticate for Jira, instead of username/password.
* Finish off CRUD methods to manage on/off-boarding.
* Add error handling for web requests.
* Add error handling for Jira responses.
* On/off-boarding tasks should be database driven.
* Make use of components for systems that staff need access to.
* The staff on/off-boarding should be an issue and each of the systems they access should be subtasks.