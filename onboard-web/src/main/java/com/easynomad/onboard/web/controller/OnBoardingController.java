package com.easynomad.onboard.web.controller;

import com.easynomad.onboard.web.model.Staff;
import com.easynomad.onboard.web.service.OnBoardingService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;
import java.util.List;

import static com.easynomad.onboard.web.controller.Endpoint.ON_BOARDING;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;

@Controller
@RequestMapping(ON_BOARDING)
public class OnBoardingController {

    private final OnBoardingService onboardingService;

    public OnBoardingController(final OnBoardingService onboardingService) {
        this.onboardingService = onboardingService;
    }

    @PostMapping(consumes = APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<Void> createOnBoarding(@Valid final Staff staff) {

        return ResponseEntity.created(URI.create(
                "%s?onboardingId=%s".formatted(ON_BOARDING, onboardingService.createOnBoardingTasks(staff))))
                             .build();
    }

    @GetMapping
    String getOnBoarding(final Model model, final String onboardingId) {

        model.addAttribute("staff", new Staff("", "", List.of()));

        if ( StringUtils.hasText(onboardingId) ) {
            model.addAttribute(onboardingService.getOnBoardingTasks(onboardingId));
        }

        return "onboarding";
    }
}
