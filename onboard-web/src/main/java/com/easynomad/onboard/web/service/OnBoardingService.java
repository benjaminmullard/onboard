package com.easynomad.onboard.web.service;

import com.easynomad.onboard.web.model.Staff;

import java.util.Map;

public interface OnBoardingService {

    String createOnBoardingTasks(Staff staff);

    Map<String, String> getOnBoardingTasks(String onboardingId);
}
