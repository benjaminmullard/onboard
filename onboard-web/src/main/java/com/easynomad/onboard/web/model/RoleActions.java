package com.easynomad.onboard.web.model;

import java.util.Map;
import java.util.Set;

import static com.easynomad.onboard.web.model.Role.DEVOPS;
import static com.easynomad.onboard.web.model.Role.FINANCE;
import static com.easynomad.onboard.web.model.Role.MARKETING;
import static com.easynomad.onboard.web.model.Action.EMAIL;
import static com.easynomad.onboard.web.model.Action.IAM;
import static com.easynomad.onboard.web.model.Action.SSO;

public class RoleActions {

    private final Map<Role, Set<Action>> actions = Map.of(DEVOPS, Set.of(EMAIL, IAM, SSO),
                                                          FINANCE, Set.of(EMAIL, SSO),
                                                          MARKETING, Set.of(EMAIL, SSO));

    public Set<Action> getAction(final Role role) {
        return actions.get(role);
    }
}
