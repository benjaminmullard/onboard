package com.easynomad.onboard.web.service;

import com.easynomad.onboard.web.model.Project;
import com.easynomad.onboard.web.model.Role;
import com.easynomad.onboard.web.model.Staff;
import com.easynomad.onboard.web.model.Task;
import com.easynomad.onboard.web.model.Task.Assignee;
import com.easynomad.onboard.web.model.Task.Content;
import com.easynomad.onboard.web.model.Task.ContentType;
import com.easynomad.onboard.web.model.Task.Description;
import com.easynomad.onboard.web.model.Task.Fields;
import com.easynomad.onboard.web.model.Task.IssueType;
import com.easynomad.onboard.web.model.Task.SubContent;
import jakarta.annotation.PostConstruct;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Service
public class OnBoardingServiceImpl implements OnBoardingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OnBoardingServiceImpl.class);

    private static final String ON_BOARDING_DESCRIPTION = "Onboarding for %s. Access to: %s";
    private static final String ON_BOARDING_SUMMARY = "Onboarding for %s";
    private static final Integer TASK_ISSUE_TYPE_ID = 10001;

    private final RestTemplate jiraClient;

    private Integer projectId;

    public OnBoardingServiceImpl(final RestTemplate jiraClient) {
        this.jiraClient = jiraClient;
    }

    @PostConstruct
    public void init() {

        final var project = getProject();

        if ( project == null ) {
            projectId = createProject(Project.DEFAULT_PROJECT);
        }
        else {
            projectId = project.id();
        }
    }

    @Override
    public String createOnBoardingTasks(final Staff staff) {

        final var taskDescription = ON_BOARDING_DESCRIPTION.formatted(
                staff.name(),
                Strings.join(staff.roles()
                                  .stream()
                                  .map(Role::getValue)
                                  .iterator(),
                             ','));

        final var task = new Task(
                new Fields(new Assignee(null),
                           new Description(List.of(new Content(List.of(new SubContent(taskDescription,
                                                                                      ContentType.text)),
                                                               ContentType.paragraph))),
                           DateTimeFormatter.ISO_DATE.format(LocalDate.now()),
                           new IssueType(TASK_ISSUE_TYPE_ID),
                           new Task.Project(projectId),
                           ON_BOARDING_SUMMARY.formatted(staff.name())));

        final var response = jiraClient.postForObject("/issue", task, String.class);

        LOGGER.info("Create Task response: {}", response);

        return response;
    }

    @Override
    public Map<String, String> getOnBoardingTasks(final String onboardingId) {

        return Map.of();
    }

    private Project getProject() {

        final var response = jiraClient.getForObject("/project/%s".formatted(Project.PROJECT_KEY), Project.class);

        LOGGER.info("Get Project response: {}", response);

        return response;
    }

    private Integer createProject(final Project project) {

        final var response = jiraClient.postForObject("/project", project, String.class);

        LOGGER.info("Create Project response: {}", response);

        return 1;
    }

    private void createBoard() {

        final var requestBody = Map.of("name", "Onboard",
                                       "type", "kanban",
                                       "location", Map.of("projectKeyOrId", "ONBOARD",
                                                          "type", "project"));

        final var response = jiraClient.postForObject("/rest/agile/1.0/board", requestBody, String.class);

        LOGGER.info("Create Board response: {}", response);
    }

    private void findBoard() {

    }
}
