package com.easynomad.onboard.web.model;

public enum Role {

    DEVOPS("DevOps"),
    FINANCE("Finance"),
    MARKETING("Marketing");

    private final String value;

    Role(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
