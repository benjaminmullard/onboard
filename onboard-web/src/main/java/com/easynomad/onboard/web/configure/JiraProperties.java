package com.easynomad.onboard.web.configure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("onboard.jira")
public class JiraProperties {

    private String apiUrl = "";
    private String apiToken = "";
    private String username = "";
    private String projectName = "onboard";
    private String boardName = "onboard";

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(final String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(final String apiToken) {
        this.apiToken = apiToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(final String projectName) {
        this.projectName = projectName;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(final String boardName) {
        this.boardName = boardName;
    }
}
