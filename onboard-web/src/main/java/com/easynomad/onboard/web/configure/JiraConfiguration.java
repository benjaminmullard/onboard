package com.easynomad.onboard.web.configure;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties(JiraProperties.class)
public class JiraConfiguration {

    private final JiraProperties jiraProperties;

    public JiraConfiguration(final JiraProperties jiraProperties) {

        Assert.hasText(jiraProperties.getApiUrl(), "API URL required in 'onboard.jira.api-url'");
        Assert.hasText(jiraProperties.getApiToken(), "Password required in 'onboard.jira.api-token'");
        Assert.hasText(jiraProperties.getUsername(), "Username required in 'onboard.jira.username'");

        this.jiraProperties = jiraProperties;
    }

    @Bean
    RestTemplate jiraClient() {

        return new RestTemplateBuilder().rootUri(jiraProperties.getApiUrl())
                                        .basicAuthentication(jiraProperties.getUsername(), jiraProperties.getApiToken())
                                        .build();
    }
}
