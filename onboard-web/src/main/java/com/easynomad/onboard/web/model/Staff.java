package com.easynomad.onboard.web.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;

import java.util.Collection;

public record Staff(@NotEmpty String name,
                    @Email String email,
                    @NotEmpty Collection<Role> roles) {
}
