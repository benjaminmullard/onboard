package com.easynomad.onboard.web.model;

import java.util.List;

public record ProjectResponse(List<Project> projects) {
}
