package com.easynomad.onboard.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record Task(Fields fields) {

    public record Fields(Assignee assignee,
                         Description description,
                         @JsonProperty("duedate") String dueDate,
                         @JsonProperty("issuetype") IssueType issueType,
                         Project project,
                         String summary) {}

    public record Assignee(String id) {}

    public record Content(@JsonProperty("content") List<SubContent> contents,
                          ContentType type) {}

    public record Description(@JsonProperty("content") List<Content> contents) {}

    public record IssueType(Integer id) {}

    public record Project(Integer id) {}

    public record SubContent(String text,
                             ContentType type) {}

    public enum ContentType {
        paragraph,
        text
    }
}
