package com.easynomad.onboard.web.controller;

import com.easynomad.onboard.web.model.Staff;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.easynomad.onboard.web.controller.Endpoint.OFF_BOARDING;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;

@Controller
@RequestMapping(OFF_BOARDING)
public class OffBoardingController {

    @PostMapping(consumes = APPLICATION_FORM_URLENCODED_VALUE)
    ResponseEntity<Void> createOffBoarding(@Valid final Staff staff) {

        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED)
                             .build();
    }

    @GetMapping
    String getOffboarding(final Model model, final String onboardingId) {

        return "offboarding";
    }
}
