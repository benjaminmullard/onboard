package com.easynomad.onboard.web.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_EMPTY)
public record Project(Integer id,
                      String key,
                      String name,
                      String description,
                      @JsonProperty("self") String url) {


    public static final String PROJECT_KEY = "ONBOARD";

    private static final String PROJECT_NAME = "OnBoard";
    private static final String PROJECT_DESCRIPTION = "Manage staff on/off-boarding";

    public static final Project DEFAULT_PROJECT = new Project(null,
                                                       PROJECT_KEY,
                                                       PROJECT_NAME,
                                                       PROJECT_DESCRIPTION,
                                                       null);
}
