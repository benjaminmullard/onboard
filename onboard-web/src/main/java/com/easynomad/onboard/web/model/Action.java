package com.easynomad.onboard.web.model;

public enum Action {

    EMAIL("Email account"),
    IAM("IAM role"),
    SSO("Single sign-on account");

    private final String value;

    Action(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
