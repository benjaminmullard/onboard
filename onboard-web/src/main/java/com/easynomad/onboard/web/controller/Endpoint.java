package com.easynomad.onboard.web.controller;

public final class Endpoint {

    private Endpoint() {
        // Hide default constructor so class cannot be instantiated.
    }

    public static final String ON_BOARDING = "/on-boarding/";
    public static final String OFF_BOARDING = "/off-boarding/";
}
