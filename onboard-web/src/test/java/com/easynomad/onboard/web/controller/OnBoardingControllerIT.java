package com.easynomad.onboard.web.controller;

import com.easynomad.onboard.web.model.Role;
import com.easynomad.onboard.web.model.Staff;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Set;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class OnBoardingControllerIT {

    private static final String STAFF_NAME = "John Doe";
    private static final String STAFF_EMAIL = "john.doe@example.com";
    private static final Set<Role> STAFF_ROLES = Set.of(Role.DEVOPS);

    private static ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    static void setUpAll() {

        objectMapper = new ObjectMapper();
    }

    @Test
    void createOnboarding() throws Exception {

        final var staff = new Staff(STAFF_NAME, STAFF_EMAIL, STAFF_ROLES);

        mockMvc.perform(MockMvcRequestBuilders.post(Endpoint.ON_BOARDING)
                                              .contentType(MediaType.APPLICATION_JSON)
                                              .content(objectMapper.writeValueAsBytes(staff)))
               .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
               .andExpect(MockMvcResultMatchers.content().json("{\"this\":\"that\"}"));
    }
}